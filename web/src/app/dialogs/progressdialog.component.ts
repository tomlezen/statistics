import {Component, Inject} from '@angular/core';
import {MD_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'progress-dialog',
  templateUrl: 'progressdialog.component.html'
})

export class ProgressDialog {
  text: string;
  constructor(@Inject(MD_DIALOG_DATA) public data: any) {
    try {
      this.text = this.data.text;
    }catch(e) {
      console.debug("ProgressDialog error: " + e);
    }
  }
}
