package tlz.statistics.model

class Result<T>{
    var status: Int = 200
    var data: T? =null
    var message: String = "ok"
}