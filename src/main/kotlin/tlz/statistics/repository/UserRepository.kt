package tlz.statistics.repository

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import tlz.statistics.model.User

@Repository
interface UserRepository: MongoRepository<User, String>