package tlz.statistics.controller

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import tlz.statistics.extensions.isSpecialChar
import tlz.statistics.model.Account
import tlz.statistics.model.Result
import tlz.statistics.model.User
import tlz.statistics.repository.AccountRepository
import tlz.statistics.repository.UserRepository
import tlz.statistics.utils.ConstUtils
import tlz.statistics.utils.StatusCodes
import java.util.*
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/api/user")
class UserController {

  private val log = LoggerFactory.getLogger(javaClass)

  @Autowired
  private lateinit var accountRepo: AccountRepository

  @Autowired
  private lateinit var userRepo: UserRepository

  @PostMapping("/login")
  fun login(@RequestBody user: User): Result<*> {
    val curUser = userRepo.findOne(user.username)
    val result = Result<Any>()
    if (curUser != null && curUser.password == user.password) {
      val account = accountRepo.findOne(user.username)
      account.lastSeen = Date().time
      result.data = account
      accountRepo.save(account)
    } else {
      result.status = StatusCodes.USERNAME_OR_PASSWORD_FAILED
      result.message = "密码错误"
    }
    return result
  }

  @PostMapping("/reg")
  fun register(request: HttpServletRequest, @RequestBody user: User): Result<*> {
    val result = Result<Any>()
    user.username = user.username.trim()
    when {
      accountRepo.exists(user.username) -> { //判断用户是否存在
        result.status = StatusCodes.USERNAME_EXISTS
        result.message = "用户名已存在，请直接登陆"
      }
      user.username.length < 2 -> { //验证用户名是否符合规则
        result.status = StatusCodes.USERNAME_TOO_SHORT
        result.message = "用户名最少2个字"
      }
      user.username.length > 10 -> { //验证用户名是否符合规则
        result.status = StatusCodes.USERNAME_TOO_LONG
        result.message = "用户名最多10个字"
      }
      user.username.isSpecialChar() -> { //验证用户名中是否含有特殊字符
        result.status = StatusCodes.USERNAME_ILLEGAL
        result.message = "用户名中含有特殊字符"
      }
      user.password.length < 6 -> { //验证密码是否符合规则
        result.status = StatusCodes.PASSWORD_ILLEGAL
        result.message = "用户密码不符合规则"
      }
      else -> {
        //创建账户
        if (userRepo.save(user) == null) {
          result.status = StatusCodes.USER_SAVE_ERROR
          result.message = ConstUtils.SERVER_ERROR
          log.info("save user failed: ${user.username}")
        } else {
          log.info("create user: ${user.username}")
//          val host = "http://${request.serverName}:${request.serverPort}"
          val host = "http://${request.getHeader("Host")}"
          val account = Account(user.username, "", "$host/img/userpic.jpg", "", Date().time)
          if (accountRepo.save(account) == null) {
            result.status = StatusCodes.USER_ACCOUNT_ERROR
            result.message = ConstUtils.SERVER_ERROR
            log.info("save account failed: $account")
            //保存账户失败则移除已创建的User
            userRepo.delete(user)
          } else {
            result.message = ConstUtils.SERVER_OK
            result.data = account
            log.info("create account: $account")
          }
        }
      }
    }
    return result
  }

}