import {RxBus} from "../utils/RxBus";
import {ConstUtils} from "../utils/ConstUtils";

declare let Materialize: any;

export function toast(...args) {
  Materialize.toast(...args);
}

export class BaseComponent {
}
