import { Component, OnInit } from '@angular/core'
import {Router} from '@angular/router';
import {BaseComponent, toast} from "../base/base.component";
import { Account} from "../models/Account";
import {LoadingUtils} from "../utils/LoadingUtils";

@Component({
  selector: 'app-homepage',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss'],
})

export class HomePageComponent extends BaseComponent implements OnInit{
  //账户信息
  account: Account;
  pageStatus: PageStatus;

  constructor(public router: Router) {
    super();
    this.pageStatus = new PageStatus();
  }

  ngOnInit(): void {

    // if(localStorage.getItem("isLoggedIn") == 'true'){
    //   this.account = JSON.parse(localStorage.getItem('account'));
    //   if(this.account){
    //     return
    //   }
    // }
    // this.router.navigate(['/login'])
  }

  /**
   * 菜单按钮点击.
   */
  onMenuClick(){
    if(this.pageStatus.menuStatus.status == 'close'){
      this.pageStatus.menuStatus.status = 'open';
      this.pageStatus.menuStatus.cls = 'material-design-hamburger__icon--to-arrow';
    }else{
      this.pageStatus.menuStatus.status = 'close';
      this.pageStatus.menuStatus.cls = 'material-design-hamburger__icon--from-arrow';
    }
  }

  /**
   * 账户设置点击.
   */
  onAccountSettingClick(){
    if(this.pageStatus.accountSettingStatus == 'true'){
      this.pageStatus.accountSettingStatus = 'false'
    }else{
      this.pageStatus.accountSettingStatus = 'true'
    }
  }
}

export class PageStatus {
  menuStatus = {status: 'open', cls: 'material-design-hamburger__icon--to-arrow'};
  accountSettingStatus = 'false';
}
