export class Result<T>{
  constructor(public status: number, public data: T, public  message: string){}
}
