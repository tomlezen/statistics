import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import {HomePageComponent} from "./home/home.component";
import {HomeMainComponent} from "./home/main/home-main.component";
import {EarThermometerComponent} from "./home/app/earthermometer/earthermometer.component";
import {SettingsComponent} from "./home/settings/settings.component";

const routes: Routes = [
  { path: '',component: IndexComponent},
  { path: 'login', component: IndexComponent },
  { path: 'home', component: HomePageComponent, children: [
    {path: '', component: HomeMainComponent},
    {path: 'main', component: HomeMainComponent},
    {path: 'earthermometer', component: EarThermometerComponent},
    {path: 'setting', component: SettingsComponent},
  ] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
