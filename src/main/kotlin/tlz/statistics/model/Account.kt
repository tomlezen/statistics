package tlz.statistics.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

/**
 * Created by tomlezen
 * Date: 2017/9/24
 * TIme: 下午6:19
 */
@Document(collection = "accounts")
class Account(@Id var name: String, var nickName: String, var imageUrl: String, var email: String, var lastSeen: Long, var grade: Int = 0)