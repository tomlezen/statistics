package tlz.statistics.utils

object StatusCodes {
  const val USERNAME_EXISTS = 101
  const val USERNAME_TOO_SHORT = 102
  const val USERNAME_TOO_LONG = 103
  const val USERNAME_ILLEGAL = 104
  const val PASSWORD_ILLEGAL = 110
  const val USERNAME_OR_PASSWORD_FAILED = 120

  const val USER_SAVE_ERROR = 501
  const val USER_ACCOUNT_ERROR = 502
}