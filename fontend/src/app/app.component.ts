import {Component, OnDestroy} from '@angular/core';
import {RxBus} from "./utils/RxBus";
import {ConstUtils} from "./utils/ConstUtils";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
  progressObservable: Observable<any>;
  isShowing: boolean;
  progressText: string;

  constructor() {
    //注册进度消息
    this.progressObservable = RxBus.register<any>(ConstUtils.KEY_ACTION_LOADING);
    this.progressObservable.subscribe(value => {
      try {
        if (value.isShowing == 'true') {
          this.isShowing = true;
          try {
            this.progressText = value.text;
          } catch (e) {
            console.log("出错啦" + e)
          }
        } else {
          this.isShowing = false
        }
      } catch (e) {
        console.log("出错啦" + e)
      }
    });
  }

  ngOnDestroy(): void {
    RxBus.unregister(ConstUtils.KEY_ACTION_LOADING, this.progressObservable)
  }
}
