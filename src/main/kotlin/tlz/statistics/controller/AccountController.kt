package tlz.statistics.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import tlz.statistics.model.Account
import tlz.statistics.model.Result
import tlz.statistics.repository.AccountRepository
import tlz.statistics.utils.StatusCodes

/**
 * Created by tomlezen
 * Date: 2017/9/24
 * TIme: 下午6:17
 */
@RestController
@RequestMapping("/api/account")
class AccountController {

  @Autowired
  private lateinit var accountRepo: AccountRepository

  @GetMapping("/{name}")
  fun findByName(@PathVariable name: String): Account {
    return accountRepo.findByName(name)
  }

  @GetMapping("/exists/{name}")
  fun exists(@PathVariable name: String): Result<*> {
    return Result<Any>().apply {
      if(accountRepo.exists(name)){
        data = name
      }
    }
  }

  @GetMapping("/test")
  fun test(): String {
    return "Hello World!"
  }

}