package tlz.statistics.model

import org.springframework.data.annotation.Id

/**
 * Created by tomlezen
 * Date: 2017/9/24
 * TIme: 下午6:40
 */
data class User(@Id var username: String, val password: String)