/**
  * Created by tomlezen
  * Date: 2017/11/12
  * TIme: 下午9:53
  */
import {NgModule} from '@angular/core'
import {Routes, RouterModule} from '@angular/router'
import {HomePageComponent} from "./home.component";

const routes: Routes = [
  {path: '', component: HomePageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class HomeRoutingModule {
}
