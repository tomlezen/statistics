export class Account{
  constructor(public name: string, public nickName: string, public imageUrl: string, public email: string, public lastSeen: number, public grade: number){}
}
