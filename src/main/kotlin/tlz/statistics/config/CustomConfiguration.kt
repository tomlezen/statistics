package tlz.statistics.config

import org.springframework.boot.autoconfigure.web.HttpMessageConverters
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.GsonHttpMessageConverter

/**
 * Created by tomlezen
 * Date: 2017/9/24
 * TIme: 下午5:15
 */
@Configuration
class CustomConfiguration {

    @Bean
    fun gsonConverters(): HttpMessageConverters {
        return HttpMessageConverters(true, listOf(GsonHttpMessageConverter()))
    }

}