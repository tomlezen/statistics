import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../services/user.service';
import {FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';
import {animate, keyframes, state, style, transition, trigger} from "@angular/animations";
import {MdDialog, MdSnackBar} from '@angular/material';
import {ProgressDialog} from "../dialogs/progressdialog.component";
import {BaseComponent} from "../base/base.component";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  animations: [trigger('pigRotate', [
    state('void', style({transform: 'rotate(0deg)'})),
    state('idle', style({transform: 'rotate(0deg)'})),
    state('hover', style({transform: 'rotate(0deg)'})),
    state('loginFailed', style({transform: 'rotate(0deg)'})),
    transition('void => *', animate('1s 1s ease-out', keyframes([
      style({transform: 'rotate(0deg)', offset: 0}),
      style({transform: 'rotate(-55deg)', offset: 0.35}),
      style({transform: 'rotate(35deg)', offset: 0.5}),
      style({transform: 'rotate(-20deg)', offset: 0.7}),
      style({transform: 'rotate(10deg)', offset: 0.9}),
      style({transform: 'rotate(0deg)', offset: 1}),
    ])),),
    transition('idle => hover', animate('1800ms ease-out', keyframes([
      style({transform: 'rotate(0deg)', offset: 0}),
      style({transform: 'rotate(30deg)', offset: 0.3}),
      style({transform: 'rotate(-20deg)', offset: 0.5}),
      style({transform: 'rotate(10deg)', offset: 0.7}),
      style({transform: 'rotate(-5deg)', offset: 0.9}),
      style({transform: 'rotate(0deg)', offset: 1}),
    ])),),
    transition('idle => loginFailed', animate('1800ms ease-out', keyframes([
      style({transform: 'rotate(0deg)', offset: 0}),
      style({transform: 'rotate(-70deg)', offset: 0.2}),
      style({transform: 'rotate(400deg)', offset: 0.7}),
      style({transform: 'rotate(360deg)', offset: 1}),
    ])),),
  ]),
    trigger('enterState', [
      state('username', style({transformOrigin: '0 0 0', transform: 'rotate3d(1, 0, 0, 0)'})),
      state('password', style({transformOrigin: '0 46px -46px', transform: 'rotate3d(1, 0, 0, 90deg)'})),
      transition('username => password', [
        style({transformOrigin: '0 0 0', transform: 'rotate3d(1, 0, 0, 0deg)'}),
        animate('400ms')
      ],),
      transition('password => username', [
        style({transformOrigin: '0 46px -46px', transform: 'rotate3d(1, 0, 0, 90deg)'}),
        animate('400ms')
      ],),
    ]),
    trigger('pageState', [
      state('login', style({transform: 'rotateY(0deg)'})),
      state('register', style({transform: 'rotateY(-180deg)'})),
      transition('login => register', [
        style({transform: 'rotateY(0deg)'}),
        animate('400ms')
      ]),
      transition('register => login', [
        style({transform: 'rotateY(-180deg)'}),
        animate('400ms')
      ]),
    ])
  ],
  providers: [UserService]
})

export class IndexComponent extends BaseComponent implements OnInit {
  isRequesting: boolean;
  actionLoginForm: FormGroup;
  actionRegForm: FormGroup;
  pigState: string;
  animationStartTime: Date;
  enterState: string;
  pageState: string;

  constructor(public router: Router, private userService: UserService, private formBuilder: FormBuilder, public dialog: MdDialog, public snackBar: MdSnackBar) {
    super();
    this.pigState = 'idle';
    this.enterState = 'username';
    this.pageState = 'login';
    this.animationStartTime = new Date();
    this.buildForm()
  }

  buildForm = function () {
    this.actionLoginForm = this.formBuilder.group({
      'username': ['', [
        Validators.required,
        Validators.minLength(2),
      ]],
      'password': ['', [
        Validators.required,
        Validators.minLength(6)
      ]]
    });
    this.actionRegForm = this.formBuilder.group({
      '_username': ['', [
        Validators.required,
        Validators.minLength(2),
      ]],
      '_password': ['', [
        Validators.required,
        Validators.minLength(6)
      ]]
    });
  };

  ngOnInit(): void {
    //判断是否已经登陆
    // if (localStorage.getItem("isLoggedin") == "true") {
    //   this.router.navigate(['/home'])
    // }
  }

  private accountExists(username: string) {
    this.toggleDialog();
    this.userService.exists(username)
      .subscribe(data => {
        if (data.data) {
          this.enterState = 'password';
        } else {
          this.snackBar.open("该用户不存在", "好的", {duration: 2000})
        }
        this.toggleDialog();
      }, error => {
        this.toggleDialog();
        this.snackBar.open("验证用户出错，请稍后再试：" + error, "好的", {duration: 4000})
      })
  }

  toggleDialog() {
    this.isRequesting = !this.isRequesting;
    if (this.isRequesting) {
      BaseComponent.showProgressDialog("Loading")
    } else {
      BaseComponent.dismissProgressDialog();
    }
  }

  //执行登陆操作
  onLoggedIn() {
    this.toggleDialog();
    let username = this.actionLoginForm.get("username").value;
    let password = this.actionLoginForm.get("password").value;
    this.actionLoginForm.setValue({'username': '', 'password': ''});
    this.userService.login(username, password)
      .subscribe(data => {
        this.toggleDialog();
        if (data.status == 200 && data.data) {
          localStorage.setItem("account", JSON.stringify(data.data));
          localStorage.setItem("isLoggedIn", "true");
          this.snackBar.open("登陆成功", "好的", {duration: 2000});
          this.toHomePage();
        } else {
          this.pigState = 'loginFailed';
          this.enterState = 'username';
          this.snackBar.open("登陆失败: " + data.message + ", code:" + data.status, "好的", {duration: 4000})
        }
      }, error => {
        this.toggleDialog();
        this.pigState = 'loginFailed';
        this.enterState = 'username';
        this.snackBar.open("登陆失败，请稍后再试: " + error, "好的", {duration: 4000})
      })
  }

  onRegister() {
    this.toggleDialog();
    let username = this.actionRegForm.get("_username").value;
    let password = this.actionRegForm.get("_password").value;
    this.userService.register(username, password)
      .subscribe(data => {
        this.toggleDialog();
        if (data.status == 200 && data.data) {
          localStorage.setItem("account", JSON.stringify(data.data));
          localStorage.setItem("isLoggedIn", "true");
          this.snackBar.open("注册成功", "好的", {duration: 2000})
          this.toHomePage();
        } else {
          this.snackBar.open("注册失败:" + data.message + ', code:' + data.status, "好的", {duration: 4000})
        }
        console.log(data);
      }, error => {
        this.toggleDialog();
        this.snackBar.open("注册失败, 请稍后再试" + error, "好的", {duration: 4000})
      })
  }

  //启动hover动画
  onPigHover() {
    let curTime = new Date();
    if (curTime.getTime() - this.animationStartTime.getTime() >= 2500 && this.pigState == 'idle') {
      this.animationStartTime = curTime;
      this.pigState = 'hover';
    }
  }

  //hover动画执行结束
  onPigAnimationDone() {
    this.pigState = 'idle';
  }

  onUsernameEnterClick() {
    this.accountExists(this.actionLoginForm.get("username").value);
  }

  onFlipClick() {
    if (this.pageState == 'register') {
      this.actionLoginForm.setValue({'username': '', 'password': ''});
      this.pageState = 'login';
    } else {
      this.actionRegForm.setValue({'_username': '', '_password': ''});
      this.pageState = 'register';
    }
  }

  toHomePage() {
    this.router.navigate(['home'])
  }
}
