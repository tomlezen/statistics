import {RxBus} from "./RxBus";
import {ConstUtils} from "./ConstUtils";
/**
 * Created by tomlezen on 2017/11/12.
 */
export class LoadingUtils {
  static show(text?: string) {
    if (text) {
      RxBus.post(ConstUtils.KEY_ACTION_LOADING, {isShowing: 'true', text: text});
    } else {
      RxBus.post(ConstUtils.KEY_ACTION_LOADING, {isShowing: 'true'});
    }
  }

  static dismiss() {
    RxBus.post(ConstUtils.KEY_ACTION_LOADING, {isShowing: 'false'});
  }
}
