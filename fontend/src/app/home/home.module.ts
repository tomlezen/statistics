/**
 * Created by tomlezen on 2017/11/12.
 */
import {CommonModule} from '@angular/common'
import {NgModule} from "@angular/core";
import {HomeRoutingModule} from "./home-routing.module";
import {HomePageComponent} from "./home.component";

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule
  ],
  declarations: [HomePageComponent]
})

export class HomeModule {

}
