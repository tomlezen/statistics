import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MdDialogModule, MdButtonModule, MdProgressSpinnerModule, MdSnackBarModule} from '@angular/material';
import {AppRoutingModule} from "./app-routing.module";
import { MaterializeModule } from 'angular2-materialize';

import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component'
import { HomePageComponent } from './home/home.component'
import { ProgressDialog } from './dialogs/progressdialog.component'
import {HomeMainComponent} from "./home/main/home-main.component";
import {EarThermometerComponent} from "./home/app/earthermometer/earthermometer.component";
import {SettingsComponent} from "./home/settings/settings.component";

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    HomePageComponent,
    HomeMainComponent,
    EarThermometerComponent,
    SettingsComponent,
    ProgressDialog,
  ],
  entryComponents: [
    ProgressDialog,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    MdButtonModule,
    MdProgressSpinnerModule,
    MdSnackBarModule,
    MdDialogModule,
    MaterializeModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
