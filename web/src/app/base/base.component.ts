import {RxBus} from "../utils/RxBus";
import {ConstUtils} from "../utils/ConstUtils";

export class BaseComponent {

  static showProgressDialog(text?: string) {
    if (text) {
      RxBus.post(ConstUtils.KEY_PROGRESS, {isShowing: 'true', text: text});
    } else {
      RxBus.post(ConstUtils.KEY_PROGRESS, {isShowing: 'true'});
    }
  }

  static dismissProgressDialog() {
    RxBus.post(ConstUtils.KEY_PROGRESS, {isShowing: 'false'});
  }

}
