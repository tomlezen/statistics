import { Injectable } from '@angular/core'
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map'
import {User} from "../models/User";

@Injectable()
export class UserService{
  private accountUrl = "/api/account/exists";
  private loginUrl = "/api/user/login";
  private registerUrl = "/api/user/reg";
  constructor(private http: Http){ }

  exists(username: string){
    return this.http.get(this.accountUrl + "/" + username)
      .map((response: Response) => response.json())
  }

  // 返回该用户的账户信息
  login(username: string, password: string){
    return this.http.post(this.loginUrl, new User(username, password))
      .map((response: Response) => response.json());
  }

  //注册
  register(username: string, password: string){
    return this.http.post(this.registerUrl, new User(username, password))
      .map((response: Response) => response.json());
  }
}
