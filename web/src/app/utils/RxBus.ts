import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";

export class RxBus{
  public static readonly instance = new RxBus();

  private sourceMap = new Map<any, Array<Subject<any>>>();
  private RxBus(){}

  /**
   * 注册.
   * @param tag
   */
  register<T>(tag: any){
    let subjectArray: Array<Subject<any>> = this.sourceMap.get(tag);
    if(!subjectArray){
      subjectArray = [];
      this.sourceMap.set(tag, subjectArray)
    }
    let source = new Subject<T>();
    subjectArray.push(source);
    return source.asObservable();
  }

  /**
   * 注销.
   * @param tag
   */
  unregisterAll(tag: any){
    this.sourceMap.delete(tag);
  }

  /**
   * 注销.
   * @param tag
   * @param {Observable<any>} observable
   */
  unregister(tag: any, observable: Observable<any>){
    let subjectArray = this.sourceMap.get(tag);
    if(subjectArray){
      subjectArray.unshift(observable as Subject<any>);
      if(subjectArray.length == 0){
        this.sourceMap.delete(tag);
      }
    }
  }

  /**
   * 发送数据.
   * @param tag
   * @param value
   */
  post(tag: any, value: any){
    let subjectArray = this.sourceMap.get(tag);
    if(subjectArray){
      subjectArray.forEach(subject => subject.next(value));
    }
  }

  static register<T>(tag: any){
    return RxBus.instance.register<T>(tag);
  }

  static unregister(tag: any, observable: Observable<any>){
    return RxBus.instance.unregister(tag, observable);
  }

  static unregisterAll(tag: any){
    return RxBus.instance.unregisterAll(tag);
  }

  static post(tag: any, value: any){
    return RxBus.instance.post(tag, value);
  }
}
