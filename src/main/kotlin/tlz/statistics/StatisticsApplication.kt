package tlz.statistics

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter

@EnableWebSecurity
@SpringBootApplication
class StatisticsApplication{ //: ResourceServerConfigurerAdapter() {
//    @Autowired
//    private lateinit var sso: ResourceServerProperties

//    override fun configure(http: HttpSecurity?) {
//        http?.authorizeRequests()
//                ?.antMatchers("/", "index")
//                ?.permitAll()
//                ?.anyRequest()
//                ?.authenticated()
//    }
}

fun main(args: Array<String>) {
    SpringApplication.run(StatisticsApplication::class.java, *args)
}
