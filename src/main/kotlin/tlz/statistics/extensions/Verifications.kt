package tlz.statistics.extensions

import java.util.regex.Pattern

/**
 * 是否含有特殊字符.
 */
fun String.isSpecialChar(): Boolean{
  val regEx = "[ _`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]|\n|\r|\t"
  return Pattern.compile(regEx).matcher(this).find()
}