package tlz.statistics.repository

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import tlz.statistics.model.Account

/**
 * Created by tomlezen
 * Date: 2017/9/24
 * TIme: 下午6:18
 */
@Repository
interface AccountRepository: MongoRepository<Account, String>{
    fun findByName(name: String): Account
}